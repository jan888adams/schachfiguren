<?php

require_once("Coordinate/ChessCoordinate.php");
require_once("Pieces/Pawn.php");
require_once("Pieces/Rook.php");
require_once("Pieces/Bishop.php");
require_once("Pieces/Queen.php");

 $pawn = new Pawn(new ChessCoordinate("B", 4), true);
 $rook = new Rook(new ChessCoordinate("A", 6), true);
 $king = new King(new ChessCoordinate("G", 1), true);
 $knight = new Bishop(new ChessCoordinate("F", 6), true);
 $queen = new Queen(new ChessCoordinate("F", 5), true);


 $targets = $pawn->getTargets();

 foreach ($targets as $target){
     echo $target . " ";
 }

 echo "<br>";

 $targets = $rook->getTargets();

foreach ($targets as $target){
    echo $target . " ";
}

echo "<br>";

$targets = $king->getTargets();

foreach ($targets as $target){
    echo $target . " ";
}

echo "<br>";


$targets = $knight->getTargets();

foreach ($targets as $target){
    echo $target . " ";
}

echo "<br>";

$targets = $queen->getTargets();

foreach ($targets as $target){
    echo $target . " ";
}