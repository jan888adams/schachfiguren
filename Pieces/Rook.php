<?php

require_once("Coordinate/ChessCoordinate.php");
require_once("Pieces/IFigure.php");
require_once("ChessField.php");

/**
 * Class Rook
 */
class Rook implements IFigure
{
    /**
     * @var ChessCoordinate
     */
    private $position;

    /**
     * @var bool
     */
    private $isWhite;

    /**
     * Rook constructor.
     * @param ChessCoordinate $position
     */
    public function __construct(ChessCoordinate $position, bool $isWhite)
    {
        $this->position = $position;
        $this->isWhite = $isWhite;
    }

    /**
     * @return array
     */
    function getTargets(): array
    {
        /** @var ChessCoordinate[] $targets */
        $targets = [];

        $columns = ChessField::getColumns();
        $rows = ChessField::getRows();

        $hasNotFindAllTargets = true;

        $indexTop = array_search($this->position->getColumn(), $columns);
        $indexBottom = array_search($this->position->getColumn(), $columns);
        $indexLeft = array_search($this->position->getRow(), $rows);
        $indexRight = array_search($this->position->getRow(), $rows);

        while ($hasNotFindAllTargets) {

            if ($indexTop < count($columns)) {
                $newColumnPosition = $columns[$indexTop];
                $topCoordinates = new ChessCoordinate($this->position->getRow(), $newColumnPosition);
                $targets[] = $topCoordinates;
                $indexTop++;
            }

            if ($indexBottom > -1) {
                $newColumnPosition = $columns[$indexBottom];
                $bottomCoordinates = new ChessCoordinate($this->position->getRow(), $newColumnPosition);
                $targets[] = $bottomCoordinates;
                $indexBottom--;
            }

            if ($indexLeft > -1) {
                $newRowPosition = $rows[$indexLeft];
                $leftCoordinates = new ChessCoordinate($newRowPosition, $this->position->getColumn());
                $targets[] = $leftCoordinates;
                $indexLeft--;
            }

            if ($indexRight < count($rows)) {
                $newRowPosition = $rows[$indexRight];
                $rightCoordinates = new ChessCoordinate($newRowPosition, $this->position->getColumn());
                $targets[] = $rightCoordinates;
                $indexRight++;
            }

            if ($indexTop > 7 && $indexBottom <= 0 && $indexLeft <= 0 && $indexRight > 7) {
                $hasNotFindAllTargets = false;
            }
        }

        // Delete current position
        $targets = array_unique($targets);
        unset($targets[0]);

        return $targets;
    }

    /**
     * @return ChessCoordinate
     */
    public function getPosition(): ChessCoordinate
    {
        return $this->position;
    }

    /**
     * @param ChessCoordinate $position
     * @return Rook
     */
    public function setPosition(ChessCoordinate $position): Rook
    {
        $this->position = $position;
        return $this;
    }

}