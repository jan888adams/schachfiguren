<?php

/**
 * Class ChessCoordinate
 */
class ChessCoordinate
{
    /**
     * @var string
     */
    private $row;
    /**
     * @var int
     */
    private $column;

    /**
     * ChessCoordinate constructor.
     * @param string $row
     * @param string $column
     */
    public function __construct(string $row, string $column)
    {
        $this->row = $row;
        $this->column = $column;
    }

    /**
     * @return string
     */
    public function getRow(): string
    {
        return $this->row;
    }

    /**
     * @return int
     */
    public function getColumn(): int
    {
        return $this->column;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
       return $this->row . $this->column;
    }

}