<?php

require_once("Coordinate/ChessCoordinate.php");
require_once("ChessField.php");
require_once("Pieces/IFigure.php");
require_once("Pieces/Rook.php");
require_once("Pieces/Bishop.php");

/**
 * Class Queen
 */
class Queen implements IFigure
{
    /**
     * @var ChessCoordinate
     */
    private $position;

    private $isWhite;

    /**
     * Rook constructor.
     * @param ChessCoordinate $position
     */
    public function __construct(ChessCoordinate $position, bool $isWhite)
    {
        $this->position = $position;
        $this->isWhite = $isWhite;
    }

    /**
     * @return array
     */
    function getTargets(): array
    {
        /** @var ChessCoordinate[] $targets */
        $targets = [];

        $rook = new Rook($this->position, $this->isWhite);
        $targets = array_merge($rook->getTargets(), $targets);

        $knight = new Bishop($this->position, $this->isWhite);

        return array_merge($knight->getTargets(), $targets);
    }

    /**
     * @return ChessCoordinate
     */
    public function getPosition(): ChessCoordinate
    {
        return $this->position;
    }

    /**
     * @param ChessCoordinate $position
     * @return Queen
     */
    public function setPosition(ChessCoordinate $position): Queen
    {
        $this->position = $position;
        return $this;
    }
}