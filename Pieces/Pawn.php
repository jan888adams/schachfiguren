<?php

require_once("Coordinate/ChessCoordinate.php");
require_once("Pieces/IFigure.php");
require_once("ChessField.php");

/**
 * Class Pawn
 */
class Pawn implements IFigure
{
    /**
     * @var ChessCoordinate
     */
    private $position;

    /**
     * @var bool
     */
    private $isWhite;

    /**
     * Pawn constructor.
     * @param ChessCoordinate $position
     */
    public function __construct(ChessCoordinate $position, bool $isWhite)
    {
        $this->position = $position;
        $this->isWhite = $isWhite;
    }

    /**
     * @return array
     */
    function getTargets(): array
    {
        $targets = [];

        if ($this->isWhite) {
            if ($this->position->getColumn() == 2) {
                $movement = 2;
            } else {
                $movement = 1;
            }
        } else {
            if ($this->position->getColumn() == 2) {
                $movement = -2;
            } else {
                $movement = -1;
            }
        }

        if (!ChessField::hasReachedTheTop($this->position)) {
            $newColumnPosition = $this->position->getColumn() + $movement;
            $targets[] = new ChessCoordinate($this->position->getRow(), $newColumnPosition);
        }

        if (!ChessField::hasReachedTheBottom($this->position)) {
            $newColumnPosition = $this->position->getColumn() - $movement;
            $targets[] = new ChessCoordinate($this->position->getRow(), $newColumnPosition);
        }

        return $targets;
    }

    public function isWhite(): bool
    {
        return $this->isWhite;
    }

    /**
     * @param ChessCoordinate $position
     * @return Pawn
     */
    public
    function setPosition(
        ChessCoordinate $position
    ): self {
        $this->position = $position;
        return $this;
    }

    /**
     * @return string
     */
    public
    function getPosition(): string
    {
        return $this->position;
    }
}