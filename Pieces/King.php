<?php

require_once("Coordinate/ChessCoordinate.php");
require_once("ChessField.php");
require_once("Pieces/IFigure.php");

/**
 * Class Pawn
 */
class King implements IFigure
{
    /**
     * @var ChessCoordinate
     */
    private $position;
    /**
     * @const int
     */
    private const MOVEMENT = 1;
    /**
     * @var bool
     */
    private $isWhite;

    /**
     * Pawn constructor.
     * @param ChessCoordinate $position
     */
    public function __construct(ChessCoordinate $position, bool $isWhite)
    {
        $this->position = $position;
        $this->isWhite = $isWhite;
    }

    /**
     * @return ChessCoordinate[]
     */
    public function getTargets(): array
    {
        /** @var ChessCoordinate[] $targets */
        $targets = [];

        if (!ChessField::hasReachedTheTop($this->position)) {
            $newColumnPosition = $this->position->getColumn() + self::MOVEMENT;
            $targets[] = new ChessCoordinate($this->position->getRow(), $newColumnPosition);
        }

        if (!ChessField::hasReachedTheBottom($this->position)) {
            $newColumnPosition = $this->position->getColumn() - self::MOVEMENT;
            $targets[] = new ChessCoordinate($this->position->getRow(), $newColumnPosition);
        }

        $currentIndex = array_search($this->position->getRow(), ChessField::getRows());

        if (!ChessField::hasReachedTheLeftEnd($this->position)) {
            $newRowPosition = ChessField::getRows()[$currentIndex + self::MOVEMENT];
            $targets[] = new ChessCoordinate($newRowPosition, $this->position->getColumn());
        }

        if (!ChessField::hasReachedTheRightEnd($this->position)) {
            $newRowPosition = ChessField::getRows()[$currentIndex - self::MOVEMENT];
            $targets[] = new ChessCoordinate($newRowPosition, $this->position->getColumn());
        }

        if(!ChessField::hasReachedTheTop($this->position) && !ChessField::hasReachedTheLeftEnd($this->position)){
            $newColumnPosition = $this->position->getColumn() + self::MOVEMENT;
            $newRowPosition = ChessField::getRows()[$currentIndex - self::MOVEMENT];
            $targets[] = new ChessCoordinate($newRowPosition, $newColumnPosition);
        }

        if(!ChessField::hasReachedTheTop($this->position) && !ChessField::hasReachedTheRightEnd($this->position)){
            $newColumnPosition = $this->position->getColumn() + self::MOVEMENT;
            $newRowPosition = ChessField::getRows()[$currentIndex + self::MOVEMENT];
            $targets[] = new ChessCoordinate($newRowPosition, $newColumnPosition);
        }

        if(!ChessField::hasReachedTheBottom($this->position) && !ChessField::hasReachedTheLeftEnd($this->position)){
            $newColumnPosition = $this->position->getColumn() - self::MOVEMENT;
            $newRowPosition = ChessField::getRows()[$currentIndex - self::MOVEMENT];
            $targets[] = new ChessCoordinate($newRowPosition, $newColumnPosition);
        }

        if(!ChessField::hasReachedTheBottom($this->position) && !ChessField::hasReachedTheRightEnd($this->position)){
            $newColumnPosition = $this->position->getColumn() - self::MOVEMENT;
            $newRowPosition = ChessField::getRows()[$currentIndex + self::MOVEMENT];
            $targets[] = new ChessCoordinate($newRowPosition, $newColumnPosition);
        }

        return $targets;
    }

    /**
     * @return bool
     */
    public function isWhite(): bool
    {
        return $this->isWhite;
    }

    /**
     * @param ChessCoordinate $position
     * @return King
     */
    public
    function setPosition(
        ChessCoordinate $position
    ): self {
        $this->position = $position;
        return $this;
    }

    /**
     * @return string
     */
    public
    function getPosition(): string
    {
        return $this->position;
    }
}