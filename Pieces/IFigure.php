<?php

/**
 * Interface IFigure
 */
interface IFigure
{
    /**
     * @return array
     */
    function getTargets(): array;
}