<?php

require_once("Coordinate/ChessCoordinate.php");
require_once("Pieces/IFigure.php");
require_once("ChessField.php");
require_once("Pieces/King.php");
require_once("Pieces/Rook.php");

/**
 * Class ChessField
 */
class ChessField
{
    /**
     * @var array
     */
    private static $columns = [1, 2, 3, 4, 5, 6, 7, 8];
    /**
     * @var array
     */
    private static $rows = ["A", "B", "C", "D", "E", "F", "G", "H"];  //  0....7  = 0.... 7  -> if [0]2 = A2
    /**
     * @var IFigure[]
     */
    private $pieces;

    /**
     * @var IFigure[][]
     */
    private $field;


    /**
     * ChessField constructor.
     * @param $pieces
     */
    public function __construct($pieces)
    {
        $this->field = [8][8];
        $this->pieces = $pieces;
        $this->movePiecesToStartPosition($pieces);
    }

    private function movePiecesToStartPosition(array $pieces){

        foreach ($pieces as $piece){

            if($piece instanceof King){
               $this->moveKingToStartPosition($piece);
            }

            if($piece instanceof Rook){
                $this->moveRookToStartPosition($piece);
            }

            if($piece instanceof Pawn){
                $this->movePawnToStartPosition($piece);
            }

        }
    }

    private function moveRookToStartPosition(Rook $rook){

        if($this->field[0][0] == null){
            $this->field[0][0] = $rook;
            $rook->setPosition(new ChessCoordinate(self::$rows[0], self::$columns[0]));
        }

        elseif($this->field[0][7] == null){
            $this->field[0][7] = $rook;
            $rook->setPosition(new ChessCoordinate(self::$rows[7], self::$columns[0]));
        }

        elseif($this->field[7][0] == null){
            $this->field[7][0] = $rook;
            $rook->setPosition(new ChessCoordinate(self::$rows[7], self::$columns[0]));
        }

        elseif($this->field[7][7] == null){
            $this->field[7][7] = $rook;
            $rook->setPosition(new ChessCoordinate(self::$rows[7], self::$columns[7]));
        }

        else{
            throw new \http\Exception\InvalidArgumentException("You have added to much rooks to the chess field");
        }
    }

    private function moveKingToStartPosition(King $king){
       if($king->isWhite() && $this->field[0][3] == null) {
           $this->field[0][3] = $king;
       }
       else{
           throw new \http\Exception\InvalidArgumentException("You have already added a white king to the chess field");
       }

       if(!$king->isWhite() && $this->field[7][3] == null){
           $this->field[0][3] = $king;
       }else {
           throw new \http\Exception\InvalidArgumentException("You have already added a black king to the chess field");
       }
    }

    private function movePawnToStartPosition(Pawn $pawn){

        $rowPosition = 0;

        if($pawn->isWhite()){

            $HasNotFindFreeSpot = true;

            while ($HasNotFindFreeSpot){

                if($rowPosition == 8){
                    $rowPosition = 0;
                }

                if($this->field[1][7] != null){
                    throw new \http\Exception\InvalidArgumentException("You have added to much white pawns to the chess field");
                }

                if($this->field[1][$rowPosition] == null){
                    $this->field[1][$rowPosition] = $pawn;
                    $pawn->setPosition(new ChessCoordinate(self::$rows[$rowPosition], self::$columns[1]));
                    $HasNotFindFreeSpot = false;
                }

            }
        }

        if(!$pawn->isWhite()){

            $HasNotFindFreeSpot = true;

            while ($HasNotFindFreeSpot){

                if($rowPosition == 8){
                    $rowPosition = 0;
                }

                if($this->field[6][7] != null){
                    throw new \http\Exception\InvalidArgumentException("You have added to much black pawns to the chess field");
                }

                if($this->field[6][$rowPosition] == null){
                    $this->field[6][$rowPosition] = $pawn;
                    $pawn->setPosition(new ChessCoordinate(self::$rows[$rowPosition], self::$columns[6]));
                    $HasNotFindFreeSpot = false;
                }

            }
        }
    }

    /**
     * @return array
     */
    public static function getColumns(): array
    {
        return self::$columns;
    }

    /**
     * @return array
     */
    public static function getRows(): array
    {
        return self::$rows;
    }

    /**
     * @param $position
     * @return bool
     */
    public static function hasReachedTheTop(ChessCoordinate $position): bool
    {
        $columns = self::$columns;
        if ($position->getColumn() == end($columns)) {
            return true;
        }

        return false;
    }

    /**
     * @param ChessCoordinate $position
     * @return bool
     */
    public static function hasReachedTheBottom(ChessCoordinate $position): bool
    {
        $columns = self::$columns;
        if ($position->getColumn() == $columns[0]) {
            return true;
        }

        return false;
    }

    /**
     * @param ChessCoordinate $position
     * @return bool
     */
    public static function hasReachedTheRightEnd(ChessCoordinate $position): bool
    {
        $row = self::$rows;
        if ($position->getRow() == end($row)) {
            return true;
        }

        return false;
    }

    /**
     * @param ChessCoordinate $position
     * @return bool
     */
    public static function hasReachedTheLeftEnd(ChessCoordinate $position): bool
    {
        $row = self::$rows;
        if ($position->getRow() == $row[0]) {
            return true;
        }

        return false;
    }

    public static function isOutOfField(ChessCoordinate $position):bool{
        $rows = self::$rows;
        $columns = self::$columns;
        if ($position->getRow() >= end($rows) && $position->getColumn() >= end($columns) &&
        $position->getRow() >= end($rows)) {
            return true;
        }
    }
}