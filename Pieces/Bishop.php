<?php

require_once("Coordinate/ChessCoordinate.php");
require_once("Pieces/IFigure.php");
require_once("ChessField.php");

class Bishop implements IFigure
{
    /**
     * @var ChessCoordinate
     */
    private $position;

    /**
     * @var bool
     */
    private $isWhite;

    /**
     * Knight constructor.
     * @param ChessCoordinate $position
     * @param bool $isWhite
     */
    public function __construct(ChessCoordinate $position, bool $isWhite)
    {
        $this->position = $position;
        $this->isWhite = $isWhite;
    }

    public function getTargets(): array
    {
        $targets = [];

        $columns = ChessField::getColumns();
        $rows = ChessField::getRows();

        $hasNotFindAllTargets = true;

        $indexTopLeft = array_search($this->position->getColumn(), $columns);
        $indexLeftTop = array_search($this->position->getRow(), $rows);

        $indexBottomLeft = array_search($this->position->getColumn(), $columns);
        $indexLeftBottom = array_search($this->position->getRow(), $rows);

        $indexTopRight = array_search($this->position->getColumn(), $columns);
        $indexRightTop = array_search($this->position->getRow(), $rows);

        $indexBottomRight = array_search($this->position->getColumn(), $columns);
        $indexRightBottom = array_search($this->position->getRow(), $rows);

         while ($hasNotFindAllTargets) {

            if ($indexTopLeft < count($columns) && $indexLeftTop > -1) {
                $newColumnPosition = $columns[$indexTopLeft];
                $newRowPosition = $rows[$indexLeftTop];
                $Coordinates = new ChessCoordinate($newRowPosition, $newColumnPosition);
                $targets[] = $Coordinates;

                $indexTopLeft++;
                $indexLeftTop--;
            }


            if ($indexBottomLeft > -1 && $indexLeftBottom > -1) {
                $newColumnPosition = $columns[$indexBottomLeft];
                $newRowPosition = $rows[$indexLeftBottom];
                $Coordinates = new ChessCoordinate($newRowPosition, $newColumnPosition);
                $targets[] = $Coordinates;

                $indexBottomLeft--;
                $indexLeftBottom--;
            }

            if ($indexTopRight < count($columns) && $indexRightTop < count($columns)) {
                $newColumnPosition = $columns[$indexTopRight];
                $newRowPosition = $rows[$indexRightTop];
                $Coordinates = new ChessCoordinate($newRowPosition, $newColumnPosition);
                $targets[] = $Coordinates;

                $indexTopRight++;
                $indexRightTop++;
            }

            if ($indexBottomRight > -1 && $indexRightBottom < count($columns)) {
                $newColumnPosition = $columns[$indexBottomRight];
                $newRowPosition = $rows[$indexRightBottom];
                $Coordinates = new ChessCoordinate($newRowPosition, $newColumnPosition);
                $targets[] = $Coordinates;

                $indexBottomRight--;
                $indexRightBottom++;
            }

            if (($indexTopLeft > 7 || $indexLeftTop < 0) && ($indexBottomLeft < 0 || $indexLeftBottom < 0) &&
                ($indexTopRight > 7 || $indexRightTop > 7) && ($indexBottomRight < 0 || $indexRightBottom > 7)) {
                $hasNotFindAllTargets = false;
            }
        }

        // Delete current position
         $targets = array_unique($targets);
         unset($targets[0]);

        return $targets;
    }

    public function isWhite(): bool
    {
        return $this->isWhite;
    }

    /**
     * @param ChessCoordinate $position
     * @return Bishop
     */
    public
    function setPosition(
        ChessCoordinate $position
    ): self {
        $this->position = $position;
        return $this;
    }

    /**
     * @return string
     */
    public
    function getPosition(): string
    {
        return $this->position;
    }
}